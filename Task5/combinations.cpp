// Task 5
// Task: Combinations
// Need to implement a simple console application which takes a text file
// as an input. Input's file format:
// 1st line - integer number (N)
// 2nd line - space-separated words
// As an output it prints into all combinations of the words from
// it with up to N words. The output is printed into output file is
// specified as a second parameter.
// Constraints:
// 1. combinations should not be duplicated - i.e. combinations like "X Y"
// and "Y X" are treated as the same ones.
// 2. words should not be duplicated - i.e. combinations like "A A" or
// "B B B" should be skipped.
// An order of the combinations in file is up to the author.
// It is expected that the code will show a good design approach,
// with possibilities of extensibility, algorithm implementation
// is effective and some basic test coverage is provided.
// Please send the result in form of MS Visual Studio solution.
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <set>
#include <assert.h>
// I don't separate to header file and implementation to easy read code
// just believe that I know how do it :)

// Create namespace if you want separate for hear and implementation
namespace CombiSpace{
  /* @brief Open, combine and save result to file
  */
  class Combinations {
  public:
    Combinations(std::string in_file, std::string out_file) :
      path_to_input_file_(in_file),
      path_to_output_file_(out_file),
      count_of_combinations_(0),
      kMaxRecurseDeep(4500) {}

    /* @brief Read file and fill in parameters
    * @return true in case of success and false otherwise
    */
    bool Read_file() {
      std::ifstream infile(path_to_input_file_);

      if (!infile) {
        return false;
      }

      if (!(infile >> count_of_combinations_)) {
        return false;
      }

      // Prevent corrupt of stack
      if (count_of_combinations_ > kMaxRecurseDeep) {
        count_of_combinations_ = kMaxRecurseDeep;
      }

      std::string word;
      while(infile >> word) {
        words_.push_back(word);
      }

      infile.close();

      return true;
    }

    /* @brief Proxy for Recursive_Combinator
    */
    void Combinate() {
      Recursive_Combinator(0);
      combination_word_.empty();
    }

    /* @brief Save data, if no data create empty file
    * @return true in case of success and false otherwise
    */
    bool Save_file() {
      std::ofstream output_file(path_to_output_file_);

      if (!output_file) {
        return false;
      }

      for (auto line:possible_combinations_) {
        output_file << line << std::endl;
      }

      output_file.close();

      return true;
    }

    /* @brief Need only for testing purpose.
    * @comment I didn't hide it under #ifdebug,
    * because don't want create 2 different code base
    * @return Return collection of combination
    */
    std::set<std::string> Possible_combinations() {return possible_combinations_;}

    /* @brief Need only for testing purpose.
    * @comment I didn't hide it under #ifdebug,
    * because don't want create 2 different code base
    * @return Return max size combined words
    */
    unsigned int Count_of_combinations(){return count_of_combinations_;}

  private:
    // Fix C4512
    Combinations &operator=(const Combinations&) {return *this;}

    /* @brief Recursively create combined words
    */
    void Recursive_Combinator(const unsigned int start_position) {
      for (auto i = start_position; i < words_.size(); i++) {
        combination_word_ += words_[i];
        possible_combinations_.insert(combination_word_);
        if (i < words_.size() &&
          combination_word_.length() < count_of_combinations_) {
            Recursive_Combinator(i + 1);
        }
        combination_word_.erase(combination_word_.length() - 1);
      }
    }

    /* @brief Contains path to input file
    */
    const std::string path_to_input_file_;

    /* @brief Contains path to output file
    */
    const std::string path_to_output_file_;

    /* @brief Amount of combined words
    */
    unsigned int count_of_combinations_;

    /* @brief Contains words that was read from file
    */
    std::vector<std::string> words_;

    /* @brief RContains all possible combinations in current condition
    */
    std::set<std::string> possible_combinations_;

    /* @brief Internal string that need for execute recursive
    */
    std::string combination_word_;

    /* @brief As I found in 3rd task, save recursive deep
    * for my machine in debug it's this value
    */
    const unsigned int kMaxRecurseDeep;
  };
} // namespace Combinations

int main_flow(int argc, char* argv[] ) {
  using namespace CombiSpace;

  if (argc < 3 || !strcmp(argv[1], "help")) {
    std::cout << "Help message:\n" \
      << " task5.exe [in_path] [out_path]" \
      << "   in_path - path to file with conditions to task" \
      << "   out_path - path with name to output file" \
      << std::endl;
    return 1;
  }

  Combinations combinator(argv[1], argv[2]);

  if (!combinator.Read_file()) {
    std::cout << "Unable to open file: " << argv[1] << std::endl;
    return 1;
  }

  combinator.Combinate();

  if (!combinator.Save_file()) {
    std::cout << "Unable to save file: " << argv[2] << std::endl;
    return 1;
  }

  return 0;
}
// Count of combinations:
// n - count of words, m - max size of combination
// N = (n!)/(m!(m-n)!) + n!/((m - 1)!((m - 1) - n)!) + n!/((m - 2)!((m - 2) - n)!) + ... // while m == 1
void SelfTest() {
  {
    char *argv[] = {"Self", "input_normal.txt", "output.txt"};
    assert(0 == main_flow(3, argv) && "Normal flow");
  }
  {
    char *argv[] = {"Self", "file_do_not_exist.txt", "output.txt"};
    assert(1 == main_flow(3, argv) && "No input file");
  }
  {
    CombiSpace::Combinations combinator
      ("input_empty.txt", "output_empty.txt");
    assert(false == combinator.Read_file() && "Input empty: open file");
    combinator.Combinate();
    assert(combinator.Possible_combinations().size() == 0
      && "Input empty: Combinate");
    assert(true == combinator.Save_file() && "Input empty: save file");
  }
  {
    CombiSpace::Combinations combinator("input_incorrect_no_number.txt",
      "output_input_incorrect_no_number.txt");
    assert(false == combinator.Read_file() &&
      "input_incorrect_no_number: open file");
    combinator.Combinate();
    assert(combinator.Possible_combinations().size() == 0 &&
      "input_incorrect_no_numbery: Combinate");
    assert(combinator.Save_file() &&
      "input_incorrect_no_number: save file");
  }
  {
    CombiSpace::Combinations combinator("input_incorrect_no_words.txt",
      "output_empty.txt");
    assert(combinator.Read_file() &&
      "input_incorrect_no_wordsy: open file");
    combinator.Combinate();
    assert(combinator.Possible_combinations().size() == 0 &&
      "input_incorrect_no_wordsy: Combinate");
    assert(combinator.Save_file() &&
      "input_incorrect_no_words: save file");
  }
  {
    CombiSpace::Combinations combinator("input_more_lines_then_need.txt",
      "output_number_more_lines_then_need.txt");
    assert(combinator.Read_file() &&
      "input_number_more_lines_then_need: open file");
    combinator.Combinate();
    std::set<std::string> combi = combinator.Possible_combinations();
    unsigned int max_comb_size = combinator.Count_of_combinations();
    for(auto l:combi) {
      assert(l.length() <= max_comb_size);
    }
    // Calculate by formula above
    assert(combi.size() == 9401 &&
      "input_number_more_lines_then_need: Combinate");
    assert(combinator.Save_file() &&
      "input_number_more_lines_then_need: save file");
  }
  {
    CombiSpace::Combinations combinator("input_more_words_then_number.txt",
      "output_more_words_then_number.txt");
    assert(combinator.Read_file() &&
      "input_more_words_then_number: open file");
    combinator.Combinate();
    // Calculate by formula above
    assert(combinator.Possible_combinations().size() == 218 &&
      "input_more_words_then_number: Combinate");
    assert(combinator.Save_file() &&
      "input_more_words_then_number: save file");
  }
  {
    CombiSpace::Combinations combinator("input_one_word.txt",
      "output_one_word.txt");
    assert(combinator.Read_file() && "input_one_word: open file");
    combinator.Combinate();
    assert(combinator.Possible_combinations().size() == 1 &&
      "input_one_word: Combinate");
    assert(combinator.Save_file() && "input_one_word: save file");
  }
}

int main(int argc, char* argv[] ) {
  if (argc == 2 && !strcmp(argv[1], "test")) {
    SelfTest();
  } else {
    main_flow(argc, argv);
  }
}