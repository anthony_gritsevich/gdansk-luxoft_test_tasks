﻿// Task 3
// Write a recursive function that calculates the Fibonacci sequence up to
// 1000 Ie. f(i) < 1000. Return the sequence in some kind of container.

#include <list>
#include <assert.h>
#include <stdint.h>

// In VS for C++ program default stack size 1 MB
// One recursive call take from stack N bytes, where N depends on build
// configuration size of function and other params for function call
// So, max safe recurse deep is 2^20/ = 1 048 576 / 230 = ~4539
// (~231 one call in debug for my machine)
// https://msdn.microsoft.com/en-us/library/8cxs58a6.aspx
//
const uint16_t kMaxFibonacciNumber = 4500; // safely stack deep
typedef std::list<uint64_t> FibList;
const uint64_t kFirstFibonacciNumber = 0;
const uint64_t kSecondFibonacciNumber = 1;

void recursive_fibonachi(
  uint64_t prev, uint64_t next,
  FibList &sequence, uint64_t &max_number) {
    if (sequence.size() > kMaxFibonacciNumber) { // Max deep
      return; // can corrupt stack
    }
    if (next >= max_number) {
      return;
    }

    sequence.push_back(next);

    return recursive_fibonachi(next, prev + next, sequence, max_number);
}

FibList fibonacci_seq(uint64_t max_fib_number) {
  FibList sequence;

  if (kFirstFibonacciNumber < max_fib_number) {
    // Pre-condition of Fibonacci sequence
    sequence.push_back(kFirstFibonacciNumber);
  }
  if (kSecondFibonacciNumber < max_fib_number) {
    // Pre-condition of Fibonacci sequence
    sequence.push_back(kSecondFibonacciNumber);
  }

  recursive_fibonachi(1, 1, sequence, max_fib_number);

  return sequence;
}

void main() {
  assert(fibonacci_seq(1000).size() == 17);
  assert(fibonacci_seq(100).size() == 12);
  assert(fibonacci_seq(10).size() == 7);
  assert(fibonacci_seq(2).size() == 3);
  assert(fibonacci_seq(1).size() == 1);
  assert(fibonacci_seq(0).size() == 0);
  // +2 precondition numbers and (-1) because 4500 do not achievable
  assert(fibonacci_seq(-1).size() == kMaxFibonacciNumber + 1);
}