﻿// Task 1
// Add up all the even elements of the Fibonacci sequence below 1000.
// Ie. f(i) < 1000
#include <assert.h>
#include <stdint.h>

uint64_t calculate_even_ibonachi_sum(const unsigned int kMaxFibSeqNumber) {
  uint64_t fibonachi_even_sum = 0;
  uint64_t prev_num = 1;
  uint64_t cur_num = 1;
  uint64_t buffer = 0;

  while (buffer < kMaxFibSeqNumber) {
    if (buffer % 2 == 0) {
      fibonachi_even_sum += buffer;
    }

    buffer = prev_num + cur_num;
    prev_num = cur_num;
    cur_num = buffer;
  }

  return fibonachi_even_sum;
}

void main() {
  // !1 + !1 + 2 = 2
  assert(calculate_even_ibonachi_sum(3) == 2);
  // !1 + !1 + 2 + !3 + !5 + 8 = 10
  assert(calculate_even_ibonachi_sum(10) == 10);
  // !1 + !1 + 2 + !3 + !5 + 8 + !13 + !21 + 34 + !55 + !89 + 144 = 188
  assert(calculate_even_ibonachi_sum(200) == 188);
  assert(calculate_even_ibonachi_sum(1) == 0);
  assert(calculate_even_ibonachi_sum(0) == 0);
  // !1 + !1 + 2 + !3 + !5 + 8 + !13 + !21 + 34 +
  // !55 + !89 + 144 + !233 + !377 + 610 + !987
  assert(calculate_even_ibonachi_sum(1000) == 798);
}